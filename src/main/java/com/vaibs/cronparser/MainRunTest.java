package com.vaibs.cronparser;

import java.text.ParseException;

public class MainRunTest {

    public static void main(String[] args) throws ParseException {
        System.out.println("input: " + args[0]);
        CronExpressionParser cronExpressionParser = new CronExpressionParser(args[0]);
        System.out.println(cronExpressionParser.getExpressionSummary());

    }
}
