package com.vaibs.cronparser;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Provides a parser for cron expressions
 * <p>
 * Cron expressions are comprised of the standard cron format with five time fields (minute, hour, day of
 * month, month, and day of week) plus a command, and it does not handle the special
 * time strings such as "@yearly". The input will be on a single line
 */
public class CronExpressionParser {

    private static final int MINUTE = 0;
    private static final int HOUR = 1;
    private static final int DAY_OF_MONTH = 2;
    private static final int MONTH = 3;
    private static final int DAY_OF_WEEK = 4;
    private static final int COMMAND = 5;

    private final TreeSet<Integer> minutes = new TreeSet<>();
    private final TreeSet<Integer> hours = new TreeSet<>();
    private final TreeSet<Integer> daysOfMonth = new TreeSet<>();
    private final TreeSet<Integer> months = new TreeSet<>();
    private final TreeSet<Integer> daysOfWeek = new TreeSet<>();
    private String command = "";

    /**
     * Constructs a new <CODE>CronExpressionParser</CODE> based on the specified
     * parameter.
     *
     * @param cronExpression String representation of the cron expression the
     *                       new object should represent
     * @throws java.text.ParseException if the string expression cannot be parsed into a valid
     */
    public CronExpressionParser(String cronExpression) throws ParseException {
        if (cronExpression == null) {
            throw new IllegalArgumentException("cronExpression cannot be null");
        }

        buildExpression(cronExpression);
    }

    private void buildExpression(String expression) throws ParseException {
        StringTokenizer exprsTok = new StringTokenizer(expression, " \t",
                false);
        int exprOn = MINUTE;
        while (exprsTok.hasMoreTokens() && exprOn <= COMMAND) {
            String expr = exprsTok.nextToken().trim();
            if (exprOn == COMMAND) {
                command = expr;
            } else {
                StringTokenizer vTok = new StringTokenizer(expr, ",");
                while (vTok.hasMoreTokens()) {
                    String v = vTok.nextToken();
                    parseAndValues(v, exprOn);
                }
            }

            exprOn++;
        }


    }

    private void parseAndValues(String s, int type) throws ParseException {
        TreeSet<Integer> set = getSet(type);
        if (!(s.contains("/") || s.contains("*") || s.contains("-"))) {
            validateInput(Integer.parseInt(s), 1, type);
            set.add(Integer.valueOf(s));
        } else if (s.equals("*")) {
            for (int i = getMin(type); i <= getMax(type); i++) {
                set.add(i);
            }
        } else if (s.contains("-")) {
            String[] parts = s.split("-");
            validateInput(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), type);
            for (int i = Integer.parseInt(parts[0]); i <= Integer.parseInt(parts[1]); i++) {
                set.add(i);
            }
        } else if (s.contains("/")) {
            int start = getMin(type);
            String[] parts = s.split("/");
            if (!parts[0].equals("*")) {
                start = Integer.parseInt(parts[0]);
            }
            validateInput(start, Integer.parseInt(parts[1]), type);
            for (int i = start; i < getMax(type); ) {
                set.add(i);
                i = i + Integer.parseInt(parts[1]);
            }
        }
    }

    private int getMax(int type) {
        int max = 0;
        switch (type) {
            case MINUTE:
                max = 60;
                break;
            case HOUR:
                max = 24;
                break;
            case MONTH:
                max = 12;
                break;
            case DAY_OF_WEEK:
                max = 7;
                break;
            case DAY_OF_MONTH:
                max = 31;
                break;
            default:
                throw new IllegalArgumentException("Unexpected type encountered");
        }

        return max;
    }

    private int getMin(int type) {
        int min;
        switch (type) {
            case MINUTE:
            case HOUR:
                min = 0;
                break;
            case MONTH:
            case DAY_OF_MONTH:
            case DAY_OF_WEEK:
                min = 1;
                break;
            default:
                throw new IllegalArgumentException("Unexpected type encountered");
        }
        return min;
    }

    private TreeSet<Integer> getSet(int type) {
        switch (type) {
            case MINUTE:
                return minutes;
            case HOUR:
                return hours;
            case DAY_OF_MONTH:
                return daysOfMonth;
            case MONTH:
                return months;
            case DAY_OF_WEEK:
                return daysOfWeek;
            default:
                return null;
        }
    }

    public String getExpressionSummary() {
        StringBuilder buf = new StringBuilder();

        buf.append(String.format("%1$-" + 14 + "s", "minutes"));
        buf.append(getExpressionSetSummary(minutes));
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "hours"));
        buf.append(getExpressionSetSummary(hours));
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "days of month"));
        buf.append(getExpressionSetSummary(daysOfMonth));
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "months"));
        buf.append(getExpressionSetSummary(months));
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "days of week"));
        buf.append(getExpressionSetSummary(daysOfWeek));
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "command"));
        buf.append(command);
        buf.append("\n");

        return buf.toString();
    }

    private void validateInput(int val, int end, int type)
            throws ParseException {

        if (type == MINUTE) {
            if (val < 0 || val > 59 || end > 59) {
                throw new ParseException(
                        "Minute values must be between 0 and 59",
                        -1);
            }
        } else if (type == HOUR) {
            if (val < 0 || val > 23 || end > 23) {
                throw new ParseException(
                        "Hour values must be between 0 and 23", -1);
            }
        } else if (type == DAY_OF_MONTH) {
            if (val < 1 || val > 31 || end > 31) {
                throw new ParseException(
                        "Day of month values must be between 1 and 31", -1);
            }
        } else if (type == MONTH) {
            if (val < 1 || val > 12 || end > 12) {
                throw new ParseException(
                        "Month values must be between 1 and 12", -1);
            }
        } else if (type == DAY_OF_WEEK) {
            if (val == 0 || val > 7 || end > 7) {
                throw new ParseException(
                        "Day-of-Week values must be between 1 and 7", -1);
            }
        }
    }

    private String getExpressionSetSummary(java.util.Set<Integer> set) {
        return set.stream().map(String::valueOf).collect(Collectors.joining(" "));
    }
}
