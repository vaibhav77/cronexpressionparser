package cronparser;

import com.vaibs.cronparser.CronExpressionParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CronExpressionParserTest {

    private static final String cronExpression = "*/15 0 1,15 * 1-5 /usr/bin/find";

    private static CronExpressionParser cronExpressionParser;

    @BeforeAll
    static void setup() throws ParseException {
        cronExpressionParser = new CronExpressionParser(cronExpression);
    }

    @Test
    void cronExpressionDescriptionTest() {
        assertEquals(getExpectedExpressionSummary(), cronExpressionParser.getExpressionSummary());

    }

    @Test
    void invalidMinuteCronExpressionDescriptionThrowsException() {
        assertThrows(ParseException.class,
                () -> {
                    new CronExpressionParser("*/79 0 1,15 * 1-5 /usr/bin/find");
                });
    }

    @Test
    void invalidHoursCronExpressionDescriptionThrowsException() {
        assertThrows(ParseException.class,
                () -> {
                    new CronExpressionParser("*/15 30 1,15 * 1-5 /usr/bin/find");
                });
    }

    @Test
    void invalidDayOfMonthCronExpressionDescriptionThrowsException() {
        assertThrows(ParseException.class,
                () -> {
                    new CronExpressionParser("*/15 0 1,45 * 1-5 /usr/bin/find");
                });
    }

    @Test
    void invalidMonthCronExpressionDescriptionThrowsException() {
        assertThrows(ParseException.class,
                () -> {
                    new CronExpressionParser("*/15 0 1,15 13 1-5 /usr/bin/find");
                });
    }

    @Test
    void invalidDayOfWeekCronExpressionDescriptionThrowsException() {
        assertThrows(ParseException.class,
                () -> {
                    new CronExpressionParser("*/15 0 1,15 * 1-8 /usr/bin/find");
                });
    }

    private String getExpectedExpressionSummary() {
        StringBuilder buf = new StringBuilder();

        buf.append(String.format("%1$-" + 14 + "s", "minutes"));
        buf.append("0 15 30 45");
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "hours"));
        buf.append("0");
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "days of month"));
        buf.append("1 15");
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "months"));
        buf.append("1 2 3 4 5 6 7 8 9 10 11 12");
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "days of week"));
        buf.append("1 2 3 4 5");
        buf.append("\n");
        buf.append(String.format("%1$-" + 14 + "s", "command"));
        buf.append("/usr/bin/find");
        buf.append("\n");

        return buf.toString();
    }
}
